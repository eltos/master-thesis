% !TeX spellcheck = en_GB

\section{Systematic study of tune signal strength} % on excitation and betatron function
\label{sec:study:excitation}

To determine the tune from the measured frequency spectra, the tune signal must be as large as possible such that it is clearly distinguishable from the background noise and any spurious signals.
In this section the dependence of the tune signal strength is investigated with respect to the excitation power, the beam intensity, and the local amplitude of the betatron function at the \gls{bpm}.
To quantify the strength of the tune signal, the amplitude~$A$ of the Gaussian fit to the tune resonance peak in the frequency spectrum is chosen as observable.
This amplitude does not include the background signal (offset).

The study is carried out with a deuteron beam at a momentum of $p=\SI{970}{MeV/c}$ consisting of \num{1.6 +- 0.03 E 10} deuterons stored in \gls{cosy}.
As can be seen in \cref{fig:excite:spectrum}, the measured tune is $Q_x = \tune{3.533+-0.012}$ and $Q_y = \tune{3.590+-0.002}$.
Since the horizontal tune is much wider, the amplitude is reduced compared to the vertical spectrum.

\begin{figure}
	\centering
	% Deuterons at 970 MeV/c
	% Particles stored: 1.60E10 +- 0.03E10
	% Beam size at IPM (1 sigma): Y=4mm, X=12mm 
	% Date: 2021-01-27 17:30:35.561030+00:00
	% Qx: 3.5334 ± 0.0117
	% Qy: 3.5902 ± 0.0018
	\includegraphics[width=\textwidth]{graphics/data/spectrum_excitation_power_scan}
	\caption[Tune measurement for the excitation study]{
		Tune measurement for the excitation study
	}\label{fig:excite:spectrum}
\end{figure}




\subsection{Dependence on excitation power}
\label{sec:study:excitation:power}

The dependence of the tune signal strength on the excitation power is studied.
Therefore, the power of the noise excitation signal is systematically changed from \SIrange{1}{12}{\milli\watt} at the signal generator output.
This corresponds to a range of \SIrange{4}{48}{\watt} applied to the beam after amplification.
For each measurement, the beam is excited over \SI{33}{ms} with a noise signal in the frequency range of \numrange{.3}{.5} times $f_\mathrm{rev}=\SI{0.75}{MHz}$ ($h=1$).
The tune is then determined using \ifglsused{fft}{an}{a} \gls{fft} over $2^{13}=\num{8192}$ sampled bunch positions.
For each setting of the excitation power, 15 measurements are performed to determine the average amplitude of the Gaussian fit as a measure for the strength of the tune resonance signal.
\\


\Cref{fig:excite:powerscan} shows the measured dependency for three exemplary \glspl{bpm} in the horizontal as well as vertical plane.
The measured amplitude~$A$ scales with the square root of the excitation power~$P$ as
\begin{equation}
\label{eq:sqrtfit}
	A(P) = p_0 \cdot \sqrt{P} + p_1
\end{equation}
The determined fit parameters~$p_i$ are listed in \cref{app:excitation}, where also additional plots for the other \glspl{bpm} are given.
%\cref{app:fig:excite:powerscan,app:tab:excitation:sqrtfitresults:x,app:tab:excitation:sqrtfitresults:y}


\begin{figure}[b!]
	\centering
	\includegraphics[scale=0.85]{graphics/data/excitation_power_scan}
	\caption[Tune signal strength in dependence of the excitation power]{
		Tune signal strength (amplitude of the Gaussian fit) in dependence of the excitation power. % applied to the beam.
		Fits are shown with a $1\sigma$ confidence interval.
	}\label{fig:excite:powerscan}
	% Deuterons at 970 MeV/c
	% Particles stored: 1.60E10 +- 0.03E10 
\end{figure}


The square root dependency can be explained by the fact, that the average power of the excitation signal %$P=1/T \int_0^T P(t) \dd{t} = 1/T \int_0^T U(t)^2/Z \dd{t}$
\begin{equation}
	P = \frac{1}{T} \int_0^T P(t) \dd{t} = \frac{1}{T} \int_0^T \frac{U(t)^2}{Z} \dd{t}
\end{equation}
scales with the square of the signal voltage~$U(t)$ where $P(t)$ is the instantaneous power, $T$ is the signal duration and $Z$ is the impedance of the stripline kicker the signal is applied to.
The resulting oscillating electric field $E(t) \propto U(t)$ between the stripline electrodes drives the betatron oscillations.
This process can be described by an ordinary harmonic oscillator driven by the force $F(t)=eE(t)$.
The betatron oscillation amplitude increases until the exciting and damping forces compensate each other.
In this steady state, the oscillation amplitude $\hat{x}$ of the harmonic oscillator is proportional to the driving force and therefore also proportional to the voltage $U \propto \sqrt{P}$.
Hence, the measured dependency of the betatron oscillation amplitude agrees with the theoretical expectation $\hat{x} \propto \sqrt{P}$.




\subsection{Dependence on betatron function}

The amplitude of the tune resonance does not only depend on the excitation power but also varies significantly between different \glspl{bpm}.
If the amplitude scaling factor~$p_0$ from the fits with \cref{eq:sqrtfit} is plotted against the \betafun{}~$\beta_{x,y}(s)$ at the location~$s$ of the respective \gls{bpm} (\cref{fig:excite:betafun}), a strong correlation becomes visible.
The values of the \betafun{} were calculated using a \gls{mad} simulation of the \gls{cosy} lattice at the time of the measurement (see \cref{app:fig:excite:twiss,app:tab:excitation:sqrtfitresults:x,app:tab:excitation:sqrtfitresults:y} in the appendix).
% model predicts tune: 
%   Q1: 3.613122443
%   Q2: 3.694591776
% measurement: see above
%   Qx: 3.533 ± 0.012
%   Qy: 3.590 ± 0.002
It has to be noted, that the simulation predicted a tune of $Q_1=\num{3.613}$ and $Q_2=\num{3.695}$ which is not in good agreement with the measured tune.
This means that the \betafun{}s predicted by the \gls{mad} model are also imprecise.
However, the deviation is expected to be negligible since the tune is the integral over the \betafun{} (\cref{eq:tune}) and therefore a small deviation in the \betafun{} results in a large deviation in tune.
\\


\begin{figure}[b!]
	\centering
	\includegraphics[scale=0.85]{graphics/data/excitation_power_scan_betafunction_scaling}
	\caption[Tune signal strength in dependence of the \betafun{}]{
		Tune signal strength (scaling factor $p_0$) in dependence of the \betafun{} at each \gls{bpm} based on a \gls{mad} model calculation.
		%Square-root fits are shown with a $1\sigma$ confidence interval.
	}\label{fig:excite:betafun}
	% Deuterons at 970 MeV/c
	% Particles stored: 1.60E10 +- 0.03E10 
\end{figure}


Despite the model uncertainty, a clear square root like dependence of the amplitude scaling factor~$p_0$ on the \betafun{} is found:
\begin{equation}
\label{eq:sqrtfit:b}
	p_0(\beta_{x,y}) = b_0 \cdot \sqrt{\beta_{x,y}} + b_1
\end{equation}
The optimized fit parameters~$b_i$ for the horizontal and vertical plane are listed below. % in \cref{tab:sqrtfitresults:b}:
The parameter~$b_1$ is negligible within the fit uncertainty.

\begin{table}[H]
	\centering
	\def\arraystretch{1.1}
	\sisetup{table-format=1.3(2)}
	\begin{tabular}{lSS}
		& {$b_0$ in \si{\arbitrary\per\sqrt{\watt\meter}}} & {$b_1$ in \si{\arbitrary}} \\ \hline
		Vertical BPMs   & 0.239+-0.017 & 0.069+-0.054 \\
		Horizontal BPMs & 0.048+-0.011 & 0.026+-0.034 \\
	\end{tabular}
	\caption{
		Fit parameters for the tune signal strength dependence
	}\label{tab:sqrtfitresults:b}
\end{table}

The functional dependence is in agreement with the theoretical description (\cref{eq:betatron}): the envelope of the betatron oscillation amplitude scales with $\hat{x}\propto\sqrt{\beta(s)}$.
It is therefore preferable to perform the measurement using \glspl{bpm} located at places with a large \betafun{}.
For the machine settings presented here, \gls{bpm} 8 is the best choice with $\beta_{x}=\SI{15.42}{m}$ and $\beta_{y}=\SI{16.63}{m}$.
In addition, the \glspl{bpm} 9, 17 and 22 have the largest \betafun{} in the vertical plane and the \glspl{bpm} 8, 20 and 13 in the horizontal plane respectively (see \cref{app:tab:excitation:sqrtfitresults:x,app:tab:excitation:sqrtfitresults:y}).




\subsection{Dependence on beam intensity}
To investigate the influence of the beam intensity, the number of particles stored in \gls{cosy} is varied by applying a \gls{pwm} at the ion source (\enquote{micro-pulsing}).
Thereby the excitation power is kept fixed at \SI{48}{\watt}.
For every setting of the beam intensity, the tune measurement is repeated at least 17 times (except for the lowest intensity where only 8 repetitions where made).
\\

\Cref{fig:excite:particlenumber} shows the result of the beam intensity study.
In order to compare different \glspl{bpm}, the measured amplitudes were normalized to the square-root of the \betafun{}.
While the amplitude of the vertical tune resonance clearly increases with higher beam intensity, the amplitude in the horizontal plane stays about constant.

% Why is that?
To discuss this behaviour, several aspects can be taken into account.
In general, an increased number of particles draws more power from the exciting transverse field.
However, this amount of power is negligibly small compared to the total excitation power even for \num{e11} particles, and therefore can not explain the observed dependence.
A higher intensity also increases the sensitivity of the \glspl{bpm} --- especially since the signal amplification is constant --- and thereby reduces the noise background.
Therefore, one would expect an increase in the amplitude~$A$.
This is apparently not the case for the horizontal plane.
%Being only present in the horizontal plane, the dispersion represents a fundamental different between the two planes. Yet there is no reason how this could have an influence on the resonance amplitude
The reason for this fundamental difference might be the distinct beam emittance, which is about 6 times larger in the horizontal plane. %due to the horizontal stacking injection used at \gls{cosy}.
% IPM measurement [at a later time for the same energy]: sigma_x=16mm, sigma_y=4mm, beta_x=15m, beta_y=6.4m --> epsilon_x=18mmmrad, epsilon_y=3mmmrad (neglecting dispersion)

To better understand this behaviour, further systematic studies are necessary to determine the effect of the beam emittance.
Additionally, the influence of the \gls{bpm}'s pre-amplifiers should be quantified independently in both planes.
\\



% Lessons learnd
In summary, the study shows that the tune measurement in the horizontal plane can not be improved by increasing the beam intensity, since this parameter only affects the vertical plane.
However, choosing a large betatron amplitude and excitation power increases the signal amplitude in both planes and can therefore be used to improve the signal-to-noise ratio.



\vfill

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.85]{graphics/data/excitation_beam_intensity_scan}
	\caption[Tune signal strength in dependence of the beam intensity]{
		Tune signal strength in dependence of the beam intensity.
		For each \gls{bpm}, the amplitude~$A$ is normalized to the value of the \betafun{}~$\beta(s)$ at the respective \gls{bpm}.
	}\label{fig:excite:particlenumber}
	% Deuterons at 970 MeV/c
\end{figure}

\vfill

%\clearpage


