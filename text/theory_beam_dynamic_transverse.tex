% !TeX spellcheck = en_GB


\section{Transverse linear beam dynamics}
\label{sec:beamdynamics:trans}

In particle accelerators, magnetic fields are used to guide the particle beam.
While dipole fields bend the trajectory onto a closed orbit,
quadrupole fields stabilize the particle motion by the effect of strong focussing~\cite[180\psqq]{Hinterberger.2008}.
The main purpose of sextupole fields is the chromaticity compensation described in \cref{sec:chromaticity_compensation}.
In some facilities like the \gls{lhc} additional higher order fields are required to counteract field errors and improve beam operation.

The ensemble and configuration of the magnets producing these fields is referred to as the \emph{lattice} of an accelerator.
It essentially defines the motion and trajectories of individual particles.





\subsection{Hill's differential equations}
\label{sec:hill_equations}

The particle motion in an accelerator is described by a set of periodic differential equations often to referred as Hill's equations~\cite[58]{Wille.1996}:
\begin{align}
	\label{eq:hill}
	\begin{aligned}
		x''(s) + \left(\frac{1}{R(s)^2} - k(s) \right) x(s) &= \frac{1}{R(s)} \frac{\Delta p}{p}
		\\[6pt]
		y''(s) + k(s) y(s) &= 0
	\end{aligned}
\end{align}
%
These describe the horizontal~$x(s)$ and vertical displacement~$y(s)$ of a particle perpendicular to the reference orbit, as a function of the longitudinal coordinate~$s$ along the orbit.
The function $R(s)$ denotes the local radius of curvature due to the dipole bending fields.
These produce a focussing force in the horizontal plane (weak geometric focussing).
The quadrupole strength~$k(s)$ describes the strong focussing.
It is present in both planes with opposite sign, since a horizontal focussing implies vertical defocussing and vice versa.
Net focussing is achieved by using drifts in between the quadrupoles (FODO structure~\cite[270]{Hinterberger.2008}).
Both $R(s)$ and $k(s)$ are periodic with respect to the orbit length~$L$.
The parameter $\Delta p$ denotes a deviation of the particle's momentum from the nominal momentum $p$.
The effect of the resulting horizontal displacement (dispersion) is described by the inhomogeneous part of the equation.





\subsection{Betatron oscillations}
\label{sec:betatron_oscillations}

For an ideal particle with $\Delta p / p = 0$ the differential equations~\ref{eq:hill} simplify and a solution can be given using the Floquet theorem~\cite[245-249]{Hinterberger.2008}:
\begin{equation}
	\label{eq:betatron}
	x(s)=\sqrt{\epsilon_x\beta_x(s)} \cdot \cos{\left(\Psi_x(s)+\Phi_x\right)}
	\quad\quad;\quad
	\Psi_x(s) = \int_{0}^{s} \frac{1}{\beta_x(z)} \dd{z}
\end{equation}
and $y(s)$ analogously.
%
The particles perform transverse oscillations, the so-called \emph{betatron oscillations}, around the reference orbit.
The oscillation amplitude depends on the longitudinal coordinate~$s$ and is described by the \betafun{}~$\beta(s)$ and the particle emittance~$\epsilon$.
The phase is composed of the phase advance~$\Psi(s)$ and the particles initial phase~$\Phi$.
While $\Phi$ and $\epsilon$ are characteristic to the individual particles and their initial conditions, the \betafun{} (and thus the phase advance) depends only on the optical properties of the accelerator, given by its magnetic lattice.
It can be obtained numerically with standard software for particle accelerator simulation, for example \gls{mad}~\cite{MAD}.
\\

In transverse phase space, the particle oscillation in $x$ and $x'$ is described by an ellipse~\cite[150,250]{Hinterberger.2008}.
While the particle travels along the orbit, it follows this phase space ellipse (\cref{fig:phasespaceellipse}), which itself continuously changes its shape, since it depends on the \betafun{} and therefore on the longitudinal coordinate~$s$.
The ellipse's area~$A=\pi\epsilon$ is proportional to the emittance and stays constant while the particle propagates (Liouville's theorem)~\cite[92]{Wille.1996}.


\begin{figure}[b!]
	\centering
	\includegraphics[scale=0.75]{graphics/theory/phase_space_ellipse}
	\caption[Phase space ellipse]{
		Particle oscillation in the horizontal phase space along the phase space ellipse.
		The position after each successive turn is indicated by the numbered dots for a fractional tune of $q=0.6$.
	}
	\label{fig:phasespaceellipse}
\end{figure}



\subsubsection*{Particle beam}
A beam is an ensemble of typically about \numrange{e8}{e12} particles performing betatron oscillations, each with a certain phase and amplitude depending on the initial conditions.
%
\begin{figure}
	\centering
	\includegraphics[scale=0.75]{theory/betatron}
	\caption[Incoherent betatron oscillations]{
		Trajectories of particles performing incoherent betatron oscillations (grey, one particle is emphasized).
		The centre of charge (beam centroid) is shown in blue and the $2\sigma$ beam envelope in red.
	}
	\label{fig:betatron_incoherent}
\end{figure}
%
\Cref{fig:betatron_incoherent} visualises a beam consisting of many individual particles.
Their betatron oscillations add up incoherently due to the random phase~$\Phi$.
As a result, the charge centroid of the beam is steady and does not oscillate.
This is an important fact when one wants to measure the betatron oscillations (see \cref{sec:tune_measurement}).

The emittance~$\epsilon$ of the particles usually follows a Gaussian distribution, which is why the beam intensity profile has a Gaussian shape.
Its width (beam size) is given by the position dependent beam envelope $E^{1\sigma}(s)=\sqrt{\epsilon^{1\sigma}\beta(s)}$ enclosing \SI{68}{\percent} of the particles~\cite[90]{Wille.1996}.
The parameter $\epsilon^{1\sigma}$ is the (1~sigma) \emph{beam emittance}.
Various definitions are used in publications and literature: as an example, \cref{fig:betatron_incoherent} shows the 2~sigma envelope containing \SI{95}{\percent} of all particles.





\subsection{Betatron tune}
\label{sec:tune}

The betatron tune is defined as the number of betatron oscillations a particle performs during a full turn in the accelerator~\cite[115]{Wille.1996}:
\begin{equation}
\label{eq:tune}
	Q
	= \frac{f_q}{f_\mathrm{rev}}
	= \frac{\Delta \Psi}{2\pi}
	= \frac{1}{2\pi} \oint \frac{1}{\beta(s)} \dd{s}
\end{equation}
were $f_q$ is the betatron oscillation frequency, $f_\mathrm{rev}$ is the revolution frequency and $\Delta\Psi = \Psi(L)$ is the phase advance for a full turn.
Often only the fractional part of the tune $q=Q-\lfloor Q \rfloor$ is stated.

The particle positions shown in \cref{fig:phasespaceellipse} correspond to a fractional tune of $q=0.6$, for example $Q=3.6$.
In this example the particle performs 3.6 transverse oscillations each turn and returns to its initial position in phase space after the 5\th{} turn and $5Q=18$ full betatron oscillations.



\subsubsection*{Betatron resonances}
\label{sec:tuneresonances}

Imperfections of the magnetic fields are unavoidable and can originate from manufacturing uncertainties, misalignments of magnets or field gradients at the beginning and end of each magnet.
Such field errors perturb the particle trajectories.
If they occur randomly, the errors average out over many turns and the trajectories remain stable.
If, however, the betatron oscillations are resonant with these perturbations, the errors add up coherently leading to an unrestricted increase of the oscillation amplitude.
As a result, the beam is lost at the acceptance limit of the accelerator.
It is therefore crucial to avoid such resonances during operation of a particle accelerator.
\cites[289-306]{Hinterberger.2008}[118-125]{Wille.1996}
\\

For dipole field errors the resonance condition is fulfilled when the particle passes the imperfection with the same betatron phase each turn (integer tune, $q=0$).
Half integer tune values ($q=1/2$) result in coherent addition of quadrupole field errors every second turn.
Higher order field errors lead to resonances at multiples of $1/n$ respectively.
While these resonances exist independently in the horizontal and vertical plane, there are also coupled resonances.
These originate from field errors affecting both planes, e.g. tilted magnets or the coupling terms of higher order multipoles.
In general, the condition for resonances of the order $\abs{m}+\abs{n}$ reads: % ~\cite[125]{Wille.1996}
\begin{equation}
	\label{eq:resonances}
	m Q_x + n Q_y = p \quad\quad ; \quad m,n,p \in \integers
\end{equation}

\Cref{fig:tunediagram} shows the tune diagram with resonance lines up to the 4\th{} order.
While the number of resonances increases with the order, their strength (width) decreases.
Typically, resonances up to the 5\th{} order have to be taken into account when choosing an appropriate working point for an accelerator in a region free of resonance lines.
\cites[125]{Wille.1996}


\begin{figure}
	\centering
	\includegraphics[scale=0.75]{graphics/theory/tune_resonances}
	\caption[Tune resonance diagram]{
		Tune diagram with resonance lines up to the 4\th{} order. The working point is marked with a black dot.
	}
	\label{fig:tunediagram}
\end{figure}



\subsection{Dispersion}
\label{sec:dispersion}

In the previous sections it was assumed that all particles have equal momentum.
However, a real beam consists of particles whose momenta are distributed around the nominal momentum $p$.
For a particle with a momentum deviation $\Delta p \neq 0$ the bending radius $R \propto p$ of the trajectory inside the dipole magnets differs.
As a result, the particle trajectory is shifted towards the out- or inside of the accelerator:
\begin{equation}
\label{eq:dispersive_orbit}
	x_p(s) = x(s) + \underbrace{D(s) \frac{\Delta p}{p}}_{x_D(s)}
\end{equation}
where $x(s)$ are the horizontal betatron oscillations (\cref{eq:betatron}) and $x_D(s)$ is the dispersive orbit shift described by the \emph{dispersion}~$D(s)$ \cite[263\psq]{Hinterberger.2008}.
As a result, the respective orbit length is in- or decreased.



\subsection{Chromaticity}
\label{sec:chromaticity}

Similar to the dipole bending radius, also the focussing strength of a quadrupole magnet~$k \propto p^{-1}$ depends on the particle's momentum.
In linear approximation, a momentum deviation therefore results in a focussing error of $\Delta k \propto -\Delta p/p$.
\\

Both, quadrupole focussing and radius of curvature, essentially define the equations of motion~\ref{eq:hill} and their solutions. % (\cref{eq:betatron,eq:dispersive_orbit}).
Therefore, a momentum deviation directly leads to a tune shift~$\Delta Q$.
This effect is called \emph{chromaticity}.
The natural\footnote{
	The term \emph{natural} indicates that the effect of sextupole magnets is not included
} chromaticity is defined as:
\begin{equation}
\label{eq:chrom:natural}
	\xi_\mathrm{nat} = \frac{\Delta Q}{\Delta p / p}
	= \frac{1}{4\pi} \oint k(s) \beta(s) \dd{s}
	%\quad < 0
\end{equation}
In certain literature\footnote{
	While the above definition is common for beam optics textbooks, many measurement related papers use $\xi$ to denote the \emph{relative} chromaticity.
	All equations in this thesis --- even if originally taken from such literature --- were modified to match the definition as per \cref{eq:chrom:natural}.
} the relative chromaticity $\xi/Q$ is used in place of $\xi$.
The natural chromaticity is always negative because stable beam operation requires net focussing, which means that the full-turn-integral $\oint k(s) \dd{s}$ must be negative.
\cites[135\psqq]{Wille.1996}[297\psqq]{Hinterberger.2008}[509\psqq]{Wiedemann.2015}



\subsubsection*{Tune spread}
\label{sec:tunespread}

In an accelerator the beam momentum is typically Gaussian distributed.
This momentum spread leads to an intrinsic width of the tune (\emph{tune spread}) by the effect of chromaticity.
For large chromaticities the tune spread can become so large that the resonances described in \cref{sec:tuneresonances} can not easily be avoided.
For stable operation of an accelerator, it may therefore be important to compensate the chromaticity.

The tune is additionally broadened by other mechanisms, mainly non-linear fields and collective effects, such as intra-beam scattering or space charge~\cite[543,723]{Wiedemann.2015}.



\subsubsection*{Chromaticity compensation with sextupoles}
\label{sec:chromaticity_compensation}

A large natural chromaticity can be compensated using sextupole magnets.
While the magnetic field strength of a quadrupole linearly increases with an offset from its centre, the field of a sextupole is proportional to the square of the offset.
This means that a sextupole's focussing strength -- which is proportional to the field gradient -- increases linearly with the horizontal\footnote{
	Rotating the magnet by \ang{30} would make the \emph{vertical} offset determining (which is undesirable)
} offset: $k_\mathrm{sex} = m x$.
\\

In order to compensate the quadrupole focussing error~$\Delta k \propto -\Delta p/p$, the sextupoles are placed in regions with large dispersion~$D$.
In the dispersive sections the momentum deviation~$\Delta p$ leads to a horizontal offset $ x_D = D \Delta p/p$.
This subsequently leads to a momentum dependent focussing in the sextupoles: $k_\mathrm{sex} \propto \Delta p/p$.
With a proper choice of the sextupole strength~$m$ the focussing error of the quadrupole magnets can thereby be compensated to achieve a small value for the chromaticity.

A single group of sextupole magnets affects the horizontal and vertical plane equally.
To compensate the chromaticity independently in both planes, at least two groups of sextupoles have to be arranged at places with different \betafun{} in the respective plane.
\cites[513\psq]{Wiedemann.2015}[299\psq]{Hinterberger.2008}
\\

Taking the effect of sextupoles into account, the \emph{total} chromaticity is:
\begin{equation}
\label{eq:chrom:total}
	\xi
	= \frac{\Delta Q}{\Delta p / p}
	= \frac{1}{4\pi} \oint \big[k(s) + m(s) D(s)\big] \beta(s) \dd{s}
\end{equation}































