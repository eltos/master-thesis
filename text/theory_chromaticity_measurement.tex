% !TeX spellcheck = en_GB


\section{Chromaticity measurement}

Classically, chromaticity is measured by slightly varying the beam momentum and measuring the corresponding tune change with either of the methods described in \cref{sec:tune_measurement}.
Since changing the momentum and tune affects beam operation, this approach might not always be feasible and alternative methods can be preferable.

Most of the methods described in this section require knowledge of the slip factor~$\eta$. % that was introduced in \cref{sec:frequency_momentum}.
It can be obtained from model calculations~\cite{MAD} or by measuring the momentum compaction factor. %~$\alpha_p=1/\gamma^2-\eta$.
% I could not not find a suitable reference for momentum compaction measurements with the b-jump method established at COSY




\subsection{Momentum change based methods}
\label{sec:chromaticity_measurement:momentum_change}

The momentum of a particle beam can be changed by adjusting either the \gls{rf} frequency or the magnetic bending field of the dipoles:


\subsubsection*{\glsentryshort{rf} frequency change}
A small change of the \gls{rf} frequency results in a momentum change proportional to the inverse slip factor $\eta^{-1}$ as described in \cref{sec:frequency_momentum}.
The chromaticity can then be determined by measuring the subsequent tune change:
\cites[339]{Wille.1996}[248]{Jones.2018}
\begin{equation}
\label{eq:chromaticity:fjump}
	\xi = \frac{\Delta Q}{\Delta p / p} = \eta\frac{\Delta Q}{\Delta f / f}
\end{equation}
Since the magnetic bending fields are kept constant during the measurement, the change in momentum also makes the beam go on an outer (or inner) orbit.
The resulting change in the horizontal beam position causes the sextupole fields to contribute to the tune change as described in \cref{sec:chromaticity_compensation}.
The measured value is consequently the \emph{total} chromaticity.
%\\


\subsubsection*{Dipole bending field change}
An alternative method to change the momentum is to adjust the magnetic bending field while keeping the \gls{rf} frequency constant.
The subsequent momentum change is given by (see \cref{app:bjump}):
\begin{equation}
	\frac{\Delta p}{p}
	= \frac{1}{1 - \gamma_\mathrm{tr}^2/\gamma^2}\frac{\Delta B}{B}
	\approx \frac{\Delta B}{B}
\end{equation}
The latter approximation is valid for energies well above transition ($\gamma \gg \gamma_\mathrm{tr}$).
In this limit the change in bending radius $R=p/(qB)$ is negligible since $\Delta R/R = \Delta p / p - \Delta B / B \approx 0$ and hence the orbit is kept constant.
This means that --- in contrast to the previously described method --- sextupole and higher order fields do not contribute to the observed tune change.
Thus this method allows to measure the \emph{natural} chromaticity instead~\cite[347]{Steinhagen.2009}.
It is again obtained from the measured tune change:
\begin{equation}
	\xi_\mathrm{nat} = \frac{\Delta Q}{\Delta p / p} \approx \frac{\Delta Q}{\Delta B / B}
\end{equation}





\subsection{Passive methods} % Non-disturbing methods

The methods described in this section do not interfere with beam operation since no active change of the beam momentum and tune is required.




\subsubsection*{Schottky monitor: width of betatron sidebands}
The transverse Schottky spectrum does not only allow to determine the tune (\cref{sec:tune_measurement:schottky}),
but also provides a non-invasive method to measure the chromaticity.
The width $\Delta f_\pm$ of the betatron sidebands at the $h$\th{} harmonic is primarily determined by the momentum spread~$\Delta p/p$, but also depends on the fractional tune~$q$ and the chromaticity~\cites[753]{Boussard.1995}:
\begin{equation}
% Note: While \cite[753]{Boussard.1995} uses \xi=(dQ/Q)/(dp/p), below \xi=(dQ)/(dp/p) is used to keep consistency
	\Delta f_\pm=f_\mathrm{rev} \frac{\Delta p}{p} \left[\left(h \pm q\right) \eta \pm \xi \right]
\end{equation}
The dependence on the chromaticity is of opposite sign for the upper and lower sideband respectively.
While the power (area) in both sidebands is equal, the width $\Delta f_+$ of the upper sideband is slightly larger than the width $\Delta f_-$ of the lower sideband (see \cref{fig:schottkyspectrum}).
The chromaticity can be reconstructed from the measured widths in the Schottky spectrum as \cite[46]{CastroCarballo.2007}:
\begin{equation}
% Note: While \cite[46]{CastroCarballo.2007} uses \xi=(dQ/Q)/(dp/p), below \xi=(dQ)/(dp/p) is used to keep consistency
	\xi = \eta \left(h \frac{\Delta f_+ - \Delta f_-}{\Delta f_+ + \Delta f_-} - q \right)
\end{equation}

While the resolution of this method is limited by the noise in the Schottky signals, its sensitivity is best for unbunched beams observed at low harmonics~\cites[250]{Jones.2018}.
Since no change of the beam momentum is required, the measurement does not interfere with beam operation and can be used for passive monitoring.



\begin{figure}[b!]
	\centering
	\includegraphics[scale=0.75]{graphics/theory/schottky}
	\caption[Transverse Schottky spectrum (schematic)]{
		Transverse Schottky spectrum (schematic)
		with revolution harmonic, betatron sidebands at $f_\pm=(h \pm q) f_\mathrm{rev}$ and respective synchrotron sidebands.
	}
	\label{fig:schottkyspectrum}
\end{figure}





\subsubsection*{Schottky monitor: amplitude of synchrotron sidebands}

For bunched beams, synchrotron sidebands can be observed in the Schottky spectrum.
The synchrotron motion in longitudinal phase space modulates the particle's momentum.
Since the chromaticity couples this momentum change to the transverse betatron motion, the tune is modulated and additional synchrotron sidebands appear on either side of the betatron sidebands as shown in \cref{fig:schottkyspectrum}.
Their amplitude scales with the chromaticity, but is effected by a number of additional factors like bunch length and synchrotron frequency.
These effects have to be quantified empirically before the chromaticity can be determined from a measurement~\cite[45]{Rehm.2010}.

Since this measurement method relies on the synchrotron motion, it is only applicable to bunched beams with a sufficient synchrotron tune such that the sidebands are clearly distinguishable from the tune resonance.
Moreover, only the magnitude but not the sign of the chromaticity can be determined.





\subsubsection*{Head-tail phase shift}

While a particle performs synchrotron oscillations, its longitudinal position inside the bunch and its momentum constantly change.
The momentum reaches a maximum at the head of the bunch and a minimum at its tail respectively.
Since the chromaticity couples this momentum difference to the betatron motion, the phase of the betatron oscillation differs between head and tail of a single bunch.
This is referred to as \emph{head-tail phase shift}~\cite{Cocq.1998}.
Depending on the sign and magnitude of the chromaticity, this phase difference can lead to labile beam motion (head-tail-instabilities~\cite{Sands.1969}), because the bunch's tail is influenced by the wake field of its head.

Although being an undesirable effect, the phase difference can be used to measure the chromaticity.
Therefore, the transverse position of the head and tail of a bunch has to be monitored separately over the course of a synchrotron period.
After the phase has been reconstructed, the phase difference $\Delta \varphi$ is calculated, which reaches a maximum after half a synchrotron period.
This maximum phase difference $\Delta\varphi_\mathrm{max}$ is proportional to the chromaticity ($\Delta\tau$ denotes the bunch length): \cite[284]{Cocq.1998}
\begin{equation}
% Note: While \cite[284]{Cocq.1998} uses \xi=(dQ/Q)/(dp/p), below \xi=(dQ)/(dp/p) is used to keep consistency
	\xi = \eta\frac{\Delta\varphi_\mathrm{max}}{4\pi f_\mathrm{rev} \Delta\tau}
\end{equation}

This method has been demonstrated~\cite{Cocq.1998}, but is not routinely used since it is difficult to determine the position of the head and tail separately.
Additionally, the phase measurement requires a sufficient bunch length and coherence during a full synchrotron period~\cite[253]{Jones.2018}.







