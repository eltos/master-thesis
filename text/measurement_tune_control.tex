% !TeX spellcheck = en_GB

\section{Tune control with quadrupole magnets}


For commissioning of the tune measurement system, the systematic adjustment of the working point using quadrupole magnets is demonstrated.
\\

The lattice of \gls{cosy} comprises a sixfold periodic ring structure and two straight telescopes (see \cref{app:fig:cosy:lattice}).
%
Each telescope is equipped with four symmetric quadrupole triplets.
A triplet consists of a focusing (F), a defocusing (D) and another focusing element, where the defocusing element needs to be twice as strong and is therefore realized with two magnets.
With a phase advance of $\Delta\Psi=2\pi$, the telescopes produce a 1:1 optical image, offering space for the beam cooling and diagnostic systems as well as internal experiments. \cites[7\psqq]{Bechstedt.1986}[184\psqq]{Hinterberger.2008}

The arcs are equipped with six mirror-symmetric FODO structures. % FODO-ODOF
The vertically focusing quadrupole magnets (F) are refereed to as MQU~1, 3 and 5; the horizontally focusing ones (vertically defocusing, D) are named MQU~2, 4 and 6.
By adjusting the focusing strength~$k$ of these quadrupole groups, the tune can be controlled and set to the desired working point.
\\

To adjust the tune, its dependence on the quadrupole strength is first quantified by measuring a tune response matrix.
The determined matrix is then used to calculate the necessary settings for the quadrupole magnets in order to reach the desired working point.
This is demonstrated in the following using a deuteron beam of \num{e10} particles at injection energy ($E_\mathrm{kin}=\SI{55}{MeV}$, $p=\SI{460}{MeV/c}$).
% http://elog.cce.kfa-juelich.de:22375/Cosy+Operating/10473






\subsection{Measurement of the tune response matrix}

The focusing strength~$k$ of a quadrupole magnet is proportional to the electric current~$I_\mathrm{MQU}$ flowing through its windings.
The relation between the current of the two quadrupole groups and the horizontal and vertical tune~$Q_{x,y}$ can be described in linear approximation by the tune response matrix~\matr{M}:
\begin{equation}
	\begin{pmatrix} \Delta Q_x \\ \Delta Q_y \end{pmatrix}
	= \matr{M} \begin{pmatrix} \Delta I_\mathrm{MQU\,1,3,5} \\ \Delta I_\mathrm{MQU\,2,4,6} \end{pmatrix}
\end{equation}

The tune response matrix is determined from tune measurements of at least three different quadrupole settings.
Therefore, the system of linear equations obtained from the pairwise difference of each two measurements is solved.
In the simplest case, three measurements are performed where only one of the two quadrupole groups is modified at a time.
The linear system is then already diagonal, and the columns of the matrix can be calculated simply by dividing $\Delta Q / \Delta I$.

\Cref{fig:mqu:tunediagram} shows such a series of tune measurements for three different settings according to \cref{app:tab:mqu:measurements}.
From these measurements, the tune response matrix follows:
\begin{equation}
	\matr{M} = \begin{bmatrix}\begin{tabular}{@{}*{2}{S[table-format=3.4(2)]@{}}}
			-0.0294 \pm 0.0050 &  0.0576 \pm 0.0031 \\
			 0.0709 \pm 0.0021 & -0.0320 \pm 0.0011
	\end{tabular}\end{bmatrix}
	\si{\per\ampere}
\end{equation}
Thereby, the uncertainty of the matrix is determined from the standard deviation of multiple tune measurements for each quadrupole setting using different \glspl{bpm}.



\begin{figure}
	\centering
	\includegraphics[scale=0.85]{graphics/data/tune_change_mqu_diagram}
	\caption[Adjustment of the working point in the tune diagram]{
		Adjustment of the working point (WP) in the tune resonance diagram.
		Tune measurements for different settings of the quadrupole magnets are shown (purple: $\Delta I_\mathrm{MQU\,1,3,5}=\SI{+0.5}{\ampere}$, red: $\Delta I_\mathrm{MQU\,2,4,6}=\SI{+0.5}{\ampere}$).
		The linear tune shift is indicated with arrows and the uncertainty on the matrix calculation is marked with an ellipse.
		%The error bars indicate the measured tune spread.
	}\label{fig:mqu:tunediagram}
\end{figure}




\subsection{Adjustment of the working point}

The required quadrupole settings for a desired tune can be calculated from the original working point $(\vec{I}_\mathrm{orig}, \vec{Q}_\mathrm{orig})$ using the inverted tune response matrix~$\matr{M}^{-1}$:
\begin{align}
	\begin{aligned}
		\begin{pmatrix} I_\mathrm{MQU\,1,3,5} \\ I_\mathrm{MQU\,2,4,6} \end{pmatrix}
		= \vec{I}_\mathrm{orig} + \matr{M}^{-1} \left[ \begin{pmatrix} Q_x \\ Q_y \end{pmatrix} - \vec{Q}_\mathrm{orig}\right]
		\\[6pt]
		\matr{M}^{-1} = \begin{bmatrix}\begin{tabular}{@{}*{2}{S[table-format=3.1(2)]@{}}}
				10.2 \pm 1.0 & 18.3 \pm 1.2 \\
				22.6 \pm 1.9 &  9.4 \pm 2.2
		\end{tabular}\end{bmatrix}
		\si{\ampere}
	\end{aligned}
\end{align}
Using this equation, new quadrupole setting for a desired tune of $Q_x=3.602$ and $Q_y=3.652$ are calculated (\cref{app:tab:mqu:measurements}).
The original and desired working point are also shown in \cref{fig:mqu:tunediagram}.
An ellipse marks the area where the new working point is expected, considering the uncertainty of the measured tune response matrix.
After applying the determined quadrupole settings, the new working point is measured to be $Q_x=\tune{3.611\pm0.008}$ and $Q_y=\tune{3.652\pm0.001}$.
This corresponds to the desired working point within the expected uncertainty.
Hence, the measurement and adjustment of the tune were demonstrated successfully.























