% !TeX spellcheck = en_GB

\section{Data analysis}


Shortly after the excitation, the acquired \bbb{} position data is read out from the Libera Hadron processors and subsequently analysed to determine the tune.
Optionally, an additional background measurement prior to the excitation can be performed.
By subtracting the background spectrum, spurious signals can be eliminated from the frequency spectra and the signal-to-noise ratio is improved, allowing for a more precise tune measurement.

With sufficiently long excitation a continuous tune measurement over several seconds can be performed.
This allows to investigate the tune evolution over time, e.g. during the acceleration ramp.



\subsection{Readout of \bbb{} data}
\label{sec:bbb-readout}

The readout of the \bbb{} data is automatically started with a sufficient delay after the data acquisition of the Libera Hadron processors was triggered by the \gls{cosy} timing system.
The positions as well as the timestamps of the detected bunches are transferred via the \gls{ca} protocol.
This protocol is a standard of the \gls{epics}, which is the control system being used at \gls{cosy}.
Since the amount of data to be transferred can become considerably large for continuous tune measurements over several seconds, the download needs to be performed in chunks.
Thereby the integrity of the data has to be ensured, especially for cases where some bunches were not detected correctly, e.g. due to low beam intensity.
This is achieved by special handling of duplicate or missing timestamps, heuristic checks for leftover data in the Libera Hadron's buffers as well as plausibility verifications on frequency, bunch number and position values.
\\

The bunch timestamps are used to select a subsample in the data corresponding to the excited betatron oscillations.
If the optional background measurement is enabled, an additional subsample prior to the excitation is also extracted for analysis.
Moreover, the revolution frequency is calculated from these timestamps. % timestamps in units of ADC clock: 4ns
\\

% how many bunches to download?
The resolution of the tune measurement is directly determined by the length of the subsample used for the spectral analysis.
For a signal consisting of $N$ bunch positions sampled at the revolution frequency, the discrete frequency spectrum is limited by the Nyquist frequency at $f_\mathrm{rev}/2$. % also called folding frequency
Therefore, the spectrum consists of $N/2$ points in the range of $r=f/f_\mathrm{rev} \in [0,0.5]$, limiting the resolution to $1/N$.

To achieve a precision of \num{e-3} (three digits), bunch positions for at least \num{1000} consecutive turns have to be acquired.
The number of turns has to be multiplied with the number of stored bunches per turn given by the harmonic $h$ of the \gls{cosy} \gls{rf}.
Rounding up to the next power of two allows to use the fast radix-2 \gls{fft} algorithm for numerical computation of the \gls{dft} (see \cref{sec:tune_measurement:bpm}).
The default subsample size used for the analysis was chosen as $N=2^{13}=\num{8192}$ bunches, but can be configured at runtime.
This ensures a high resolution even for $h=4$ while keeping the required measurement time and computational costs low.
Considering a revolution frequency from \SIrange{0.5}{1.6}{MHz} and $1 \le h \le 4$ for \gls{cosy}, the required measurement time ranges from \SIrange{2.5}{33}{ms}.



\subsection{Fourier transform and tune resonance fit}
\label{sec:fft-fit}
\glsreset{dft}

A \gls{dft} is performed on the bunch position subsamples by means of \ifglsused{fft}{an}{a} \gls{fft} algorithm implemented using the NumPy library~\cites{Numpy.fft}{Cooley.1965}.
This process is repeated for both, the horizontal and vertical plane, as well as the optional background measurement.
After subtraction of the background, the spectra are smoothened by a moving average filter to reduce the residual noise and filter out spikes.
%
As a result, the frequency spectrum normalized to the bunch frequency is obtained.
Multiplication with the number of stored bunches~$h$ yields the resonance spectrum in units of $r=f/f_\mathrm{rev}$.
The spectrum is cropped to the first half interval from \numrange{0}{0.5} since the $h-1$ repeated mirror images above $r=0.5$ contain no additional information.
% One could instead determine the tune for each bunch saparately, however since the tune is a property of the machine lattice it is equal for each bunch
\\


\begin{figure}[b]
	\centering
	\includegraphics[width=\textwidth]{graphics/data/spectrum_fitting_example}
	\caption[Frequency spectra of a tune measurement with Gaussian fits]{
		Frequency spectra of a tune measurement with Gaussian fits (close-ups)
	}\label{fig:tune_fit}
\end{figure}



\Cref{fig:tune_fit} shows the obtained frequency spectra for an exemplary tune measurement.
The non-vanishing width of the betatron resonance peak is the result of the mechanisms described in \cref{sec:tunespread}.
Since it is the superimposed signal of many independent particles, the peak can adequately be described by a normal Gaussian distribution according to the central limit theorem.
The chromaticity induced tune broadening is typically also of Gaussian shape since the beam momentum is usually normal distributed.

Using a least square optimisation, a Gaussian distribution is fitted to each of the obtained spectra as shown by the close-ups in \cref{fig:tune_fit}.
This yields the fraction~$\hat{r}$ as the mean of the distribution, from which the tune can be deduced as $q=\hat{r}$ or $q=1-\hat{r}$ (see \cref{sec:pickup_fourier}).
The distinction is made based on the \emph{tune search range}, which can be adjusted by the operator. This range is also used to limit the fit to a certain region of the spectra and to calculate the absolute tune from the fractional one.
The default tune search range for the lattice of \gls{cosy} is $Q\in[3.5,3.7]$ corresponding to a fitting range of $r\in[0.3,0.5]$ and an absolute tune of $Q=4-\hat{r}$.
%
The fit is only accepted if the fitted tune peak is significantly outside the noise level, that is, if the peak amplitude is larger than six standard deviations of the background signal.
The measurement uncertainty is computed by summation in quadrature of the fit uncertainties and the intrinsic tune width (standard deviation of the Gaussian).
In the following this uncertainty is denoted with a \enquote{$\pm$} to emphasize that it also includes the tune spread. % \tune{}

The procedure is repeated separately for both planes yielding the horizontal tune~$Q_x$ and the vertical tune~$Q_y$ with respective uncertainties.
Finally, the values and uncertainties of all successful measurements across multiple \glspl{bpm} are combined.
\\




For cases where the transverse motion is coupled\footnote{
	Coupling can occur due to rotated magnetic fields or in presence of longitudinal fields.
	The latter are generated by solenoids as used for electron cooling --- a routine operation at \gls{cosy}.
}, the spectra can contain two resonance peaks with different intensities each (\cref{fig:coupled_tunes}).
To distinguish the two tunes $Q_1$ and $Q_2$ the search range can be adjusted accordingly.
An option to determine both tunes from a single measured plane is also implemented.
One has to note, however, that for strong coupling and $Q_x \approx Q_y$, the two measured tunes $Q_1$ and $Q_2$ do not correspond to the vertical and horizontal tune but will stay separated.~\cite[692\psqq]{Wiedemann.2015}



\begin{figure}[b!]
	\centering
	\includegraphics[width=\textwidth]{graphics/data/spectrum_coupling_example}
	\caption[Frequency spectra of a tune measurement with coupling]{
		Frequency spectra of a tune measurement with coupling (logarithmic scaling).
		The horizontal tune at $Q_x=\tune{3.563\pm0.003}$ couples to the vertical plane and appears as a second smaller peak in the vertical spectrum.
	}\label{fig:coupled_tunes}
\end{figure}




\subsection{Continuous tune measurement}
\label{sec:continuous-tune}

The tune can also be monitored continuously for several seconds by exciting the betatron oscillations during the complete timeframe of interest.
This way the tune can be tracked e.g. during the acceleration ramp.





\begin{figure}[p!]
	\centering
	%\includegraphics[width=\textwidth]{graphics/data/tune_continuous_spectrogram}
	\includegraphics[width=\textwidth]{graphics/data/tune_continuous_spectrogram_with_bct}
	\caption[Tune spectrogram]{
		% http://elog.cce.kfa-juelich.de:22375/Cosy+Operating/9725
		Tune spectrogram over \SI{2}{s} for a proton beam at $p=\SI{521}{\mega\electronvolt\per\c}$.
		The visible tune change was caused by accelerating the beam by \SI{0.6}{\mega\electronvolt\per\c} between \SIrange[range-units=single,range-phrase={ and }]{0.5}{1.5}{s} after the trigger.
		The crossing of the vertical $2/3$ resonance at $t=\SI{1.3}{s}$ causes a significant beam loss (lower plot).
	}\label{fig:continuous_tunes}
\end{figure}

\begin{figure}[p!]
	\centering
	\includegraphics[scale=0.8]{graphics/data/tune_continuous_tunediagram}
	\caption[Tune diagram for the measurement in \cref{fig:continuous_tunes}]{
		Tune diagram for the measurement in \cref{fig:continuous_tunes}.
		The arrow indicates the direction of time.
		While the horizontal tune increases, the vertical tune decreases and crosses the $2/3$ resonance (yellow line).
	}\label{fig:continuous_tunes_diagram}
\end{figure}



For the continuous tune measurement, \bbb{} positions with $N=\order{\num{e6}}$ samples are loaded from the Libera Hadron for a single \gls{bpm} of choice.
The time dependent frequency spectra (spectrograms, \cref{fig:continuous_tunes}) are calculated via a \gls{stft}:
The data is divided into segments, each of which is Fourier transformed similarly to the ordinary tune measurement.
This is implemented using the SciPy library~\cites[714\psqq]{Oppenheim.1999}{SciPy.spectrogram}.

The number of bunch positions per segment $N_\mathrm{seg}$ does not only determine the resolution in tune $\delta q = 1/N_\mathrm{seg}$ as described above;
It also determines the time resolution of the measurement: $\delta t = N_\mathrm{seg} / f_\mathrm{rev}$.
Since the product $\delta q \cdot \delta t = 1/f_\mathrm{rev}$ is fixed by the revolution frequency, a reasonable compromise has to be found.
With $N_\mathrm{seg}=2^{10}=\num{1024}$ at a typical frequency of $f_\mathrm{rev}\approx\SI{1}{MHz}$ a resolution of three digits in tune and \SI{1}{ms} in time can be achieved.
This default value can be adjusted by the operator as needed for the specific application.
\\


\Cref{fig:continuous_tunes} shows an exemplary tune spectrogram for both planes in which the tunes are clearly visible as bold lines.
Each spectrum is fitted as described above, yielding the tune in its time evolution as visualized in the tune diagram (\cref{fig:continuous_tunes_diagram}).
For the given example, the tune change was induced by slightly increasing the beam momentum.
Thereby, the vertical tune crosses the $2/3$ resonance.
The resulting beam loss is clearly visible as a reduction in the number of stored particles (lower plot of \cref{fig:continuous_tunes}).
However, the continuous excitation over several seconds itself also reduces the particle number from \numrange{5E9}{4E9} over \SI{1}{s}.



To reduce the excitation induced beam loss for continuous tune measurements, a \gls{pwm} is implemented to reduce the average excitation power.
By specifying a duty cycle for the noise sample, a pulsed excitation can be achieved.
As a result, the tune appears as a dashed line in the spectrograms, which also helps to clearly distinguish the tune resonance from other spurious signals.
\\

The continuous tune measurement also provides the basis for the chromaticity measurement, which is discussed in \cref{dev:chrom}.






