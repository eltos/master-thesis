% !TeX spellcheck = en_GB


\section{Tune measurement}
\label{sec:tune_measurement}

To measure the betatron tune, the betatron oscillation frequency has to determined with respect to the revolution frequency.
Therefore, a \pickup{} structure sensitive to the transverse beam position is used.
Since the oscillation amplitude is usually too small to get a sufficient signal, the betatron oscillations are resonantly excited.
The fluctuations of the \pickup{} signal then allow to reconstruct the transverse oscillations.
Their frequency is determined after a Fourier transform of the time structure into frequency space.
\cites{Jones.2018}{Steinhagen.2009}





\subsection{Excitation of betatron oscillations}

As shown in \cref{sec:betatron_oscillations} and \cref{fig:betatron_incoherent}, the particle oscillations inside a beam are incoherent and the remnant coherent fraction is tiny.
The oscillation of the beam centroid is typically in the sub-micrometer range~\cite[244]{Jones.2018}, which is below the sensitivity of most detector systems.
External excitation is required to achieve a sufficient signal-to-noise ratio when measuring the betatron oscillation amplitude using a beam position sensitive device.

The excitation enforces a common phase on the particles such that their individual oscillations become coherent as depicted in \cref{fig:betatron_coherent}.
As a result, the beam centroid starts to oscillate and these oscillations can be measured.
\\

\begin{figure}[b!]
	\centering
	\includegraphics[scale=0.75]{graphics/theory/betatron_phaselocked}
	\caption[Coherent betatron oscillations]{
		Trajectories of particles performing coherent betatron oscillations.
		As a result, the beam centroid also oscillates.
	}
	\label{fig:betatron_coherent}
\end{figure}

The beam can either be excited by applying a single kick or \ifglsused{rf}{an}{a} \gls{rf} signal to the beam.
In the latter case the excitation frequency~$f_\mathrm{ex}$ has to be resonant with the beam oscillation: $f_\mathrm{ex}=(n \pm q) f_\mathrm{rev} ~,~ n \in \integers$.
Since the tune~$q$ is not known a priori, one can either vary the frequency by sweeping it across the range where the tune is expected, or use a signal composed of frequencies in the expected range (band limited white noise).
The \gls{rf} excitation has the advantage, that by choosing an appropriate bandwidth, the spectral power density can be increased and excitation of other unwanted resonances can be avoided~\cite[330]{Steinhagen.2009}.
\\

After the excitation is switched off, the individual oscillations start to diverge due to the differences in their momenta until they become incoherent again. % via chromaticiy
Therefore, the measurement is only possible while the excitation is active and during the typically short decoherence time afterwards.





\subsection{\Pickup{} signal and its Fourier transform}
\label{sec:pickup_fourier}

Each time a bunch of particles passes the \pickup{} structure, a short signal pulse proportional to the transverse position is measured.
The delta signal of the \pickup{} shows a train of pulses evenly spaced in time by the revolution period~$T_\mathrm{rev}=1/f_\mathrm{rev}$ as depicted by the continuous signal in \cref{fig:pick_up_and_fourier:time}.
Since the transverse position oscillates around the (non-zero) beam position, the pulse height is modulated with the (fractional) betatron frequency~$f_q = q f_\mathrm{rev}$.
% Actually $f_q = Q f_\mathrm{rev}$, but the integer part does not affect the modulation
\\

\begin{figure}[b!]
	\centering
	\begin{subfigure}{0.5\textwidth}
		\centering\captionsetup{width=.9\linewidth}
		\includegraphics[scale=0.75]{graphics/theory/fourier_timedomain}
		\caption{Signal in time domain with the pulse height being modulated by the betatron oscillation.
		}
		\label{fig:pick_up_and_fourier:time}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\centering\captionsetup{width=.9\linewidth}
		\includegraphics[scale=0.75]{graphics/theory/fourier_frequencydomain}
		\caption{Signal in frequency domain with revolution harmonics (red) and betatron sidebands (orange).
		}
		\label{fig:pick_up_and_fourier:frequency}
	\end{subfigure}
	\centering
	\caption[\Pickup{} signal in time and frequency domain]{
		Difference signal at a \pickup{} for an off-centre bunched beam performing betatron oscillations.
		While a continuous signal in time leads to a unbounded frequency spectrum,
		discrete turn-by-turn sampling reduces the observable frequency range to the region shaded in green.
	}
\end{figure}



As derived in \cref{app:fft}, the Fourier transform of this continuous signal leads to the frequency spectrum shown in \cref{fig:pick_up_and_fourier:frequency}.
The peaks at the revolution harmonics~$h f_\mathrm{rev}$ are accompanied by betatron sidebands located at $f_\pm = h f_\mathrm{rev} \pm f_q$ which originate from the modulation of the pulse height.
The distance of these sidebands to the corresponding harmonics --- normalized to the revolution frequency --- corresponds to the fractional tune:
\cite[415\psqq]{Caspers.2009}
\begin{equation}
	q = \frac{f_q}{f_\mathrm{rev}} = \frac{f_+ - f_-}{2f_\mathrm{rev}}
\end{equation}
%\\



Instead of the continuous \pickup{} signal, it is also possible to use the time discretised beam positions from a \gls{bpm}.
Here, only one position value is obtained each turn (turn-by-turn sampling).
The Nyquist-Shannon theorem states that for a signal sampled at a frequency~$f_\mathrm{s}$, only frequencies $f<f_\mathrm{s}/2$ can be reconstructed.
For the case of turn-by-turn sampling this reduces the observable frequency range to the first half of the revolution frequency, as indicated by the shaded region in \cref{fig:pick_up_and_fourier:frequency}.
In this limited region, only a single betatron sideband appears.
To determine the tune, one has to decide whether the observed sideband is the upper sideband at $f_+=qf_\mathrm{rev}$ or the lower sideband of the first harmonic at $f_-=(1-q)f_\mathrm{rev}$. % 1st harmonic = fundamental frequency
In general $q$ and $1-q$ can not be distinguished from a single measurement.
The ambiguity can be resolved by varying the tune in a controlled manner and observing in which direction the betatron sideband moves~\cite[242]{Jones.2018}.
However, usually this is not required since the approximate range of the tune is known from model calculations.






\subsection{Different measurement methods}

Implementations of tune measurements vary by the type of excitation, the hard- and software used and the way the tune is determined from the frequency or phase information.
In the following an overview is given on the various methods.



\subsubsection*{Schottky monitor}
\label{sec:tune_measurement:schottky}

Schottky diagnostics trace back to the physicist Walter Schottky, who first described the concept of current fluctuations (shot noise) in 1918.
A Schottky monitor exploits this concept by detecting the current fluctuations of a particle beam with a \pickup{} structure.
Using a spectrum analyser, these fluctuations are observed as distinct bands in the frequency spectrum.
Since the longitudinal spectrum obtained from the sum signal of a \pickup{} is dominated by the momentum fluctuations, it can be used to gain information on the momentum distribution and synchrotron motion.
Fluctuations of the transverse beam position are contained in the delta signal (see \cref{sec:bpm}).
The corresponding transverse spectrum therefore provides information on the transverse momentum distribution and betatron motion.
\cite{Betz.2017}
\\

When the betatron oscillations are excited with broadband noise, a Schottky monitor connected to the delta signal of a suitable \pickup{} can be used to observe the betatron sidebands at $f_\pm$.
For unbunched (continuous) beams the revolution harmonics are absent, but for bunched beams with a sufficient transverse offset they can still be observed.
If the spectrum analyser is configured to display the frequency spectrum around the $h$\th{} harmonic, the tune is given by~\cite[416]{Caspers.2009}:
\begin{equation}
	q
	= \frac{f_+ - f_-}{2 f_\mathrm{rev}}
	= h \frac{f_+ - f_-}{f_+ + f_-}
\end{equation}
\\
The advantage of a Schottky monitor is its applicability to bunched as well as continuous beams.
Depending on the sensitivity of the \pickup{} used, the excitation can be relatively low.
However, to get a reasonable good resolution the acquisition time has to be in the order of a second, which is relatively slow compared to other methods.

Besides the tune, a Schottky monitor can also be used to determine a variety of other parameters --- revolution frequency, beam momentum, momentum spread, synchrotron tune or chromaticity --- which makes it a multi-purpose diagnostic tool.
\cite{Caspers.2009}



\subsubsection*{Regular \glsentrylong{bpm}}
\label{sec:tune_measurement:bpm}

With sufficient excitation, a standard \gls{bpm} can be used to detect the oscillation of the beam centroid.
The position is determined each time the particle bunch passes by (turn-by-turn sampling).
The sampled beam positions undergo a \gls{dft} yielding the frequency spectrum.
The tune is finally extracted from the sideband in the normalized frequency spectrum as described in \cref{sec:pickup_fourier}.
\\

For efficient computation of the \gls{dft}, \gls{fft} algorithms exist, which iteratively decompose the calculations into smaller parts.
For a signal of $N$ samples, \textcquote[231]{Plonka.2018}{the FFTs considerably reduce the computational cost for computing the DFT($N$) from $2N^2$ to $\order{N \log{N}}$ arithmetic operations}.
A particularly well known algorithm is the radix-2~FFT by Cooley and Tukey~\cite{Cooley.1965} where the calculations are iteratively split into half.
To make use of this algorithm, the number of sampled beam positions must be a power of two ($N=2^n$).
\\

Since \glspl{bpm} exist in any accelerator, this method does not require additional hardware and is therefore cost saving.
However, it is only applicable to bunched beams and typically requires large excitation power, since the dynamic range of a \gls{bpm} is usually small.
\cite[244]{Jones.2018}



\subsubsection*{\Glsentrylong{bbq} system}
\glsreset{bbq}

To overcome the dynamic range limit of a regular \gls{bpm}, a dedicated analogue signal processing is needed, 
sensitive only to changes of the beam position, but indifferent regarding the absolute beam position.
Such a system is the \gls{bbq} system~\cite{Gasior.2005}, which is able to resolve sub-micrometer betatron oscillations.
Since remnant beam oscillations of this magnitude are typically present, only a very weak or even no excitation at all is required for the measurement. \cite[244\psq]{Jones.2018}

While such a system allows for non-invasive and therefore continuous monitoring of the tune, it is typically less precise than methods relying on a strong excitation due to the relatively large noise level.



\subsubsection*{\Glsentrylong{btf} measurement}
\glsreset{btf}

In a \gls{btf} measurement the beam is excited with a sinusoidal signal of precisely known frequency and phase.
The frequency is swept over the range of interest, while the amplitude and phase of the betatron oscillations are recorded as a function of the frequency, e.g. by means of a network analyser~\cite[775]{Boussard.1995}.
The tune resonance can not only be derived from the peak in the amplitude spectrum, but also from the point of maximum slope in the phase response at $\varphi=\pi/2$.

Sweeping the excitation frequency allows for a precise determination of the phase and therefore increases the measurement accuracy.
However, it also requires more time and makes this measurement method slow.
The time resolution can be improved by sweeping the frequency rapidly (chirp excitation) on the cost of a less precise measurement~\cite[43\psq]{Schmickler.1997}.



\subsubsection*{\Glsentrylong{pll} tune tracker}
\glsreset{pll}

A \gls{pll} tune tracker continuously adjusts the excitation frequency to match the betatron tune~\cite[615\psqq]{Tan.2006}.
The control variable of such a closed-loop system is the phase of the beam oscillation, which is regulated to match the resonance condition $\varphi=\pi/2$.
Such a system allows to track the time evolution of the tune very precisely with a resolution in the order of \SI{e-6}{}~\cite[341]{Steinhagen.2009}.
Yet it requires a sophisticated regulation circuit so as not to get locked at parasitic resonances.










