% !TeX spellcheck = en_GB

\section{Chromaticity measurement and compensation}

The chromaticity measurement system is commissioned using a machine configuration similar to the experimental setup used by the \gls{jedi} collaboration.
A deuteron beam is accelerated to \SI{970}{MeV/c} and cooled in all six dimensions of phase space using electron cooling.
Thereby the momentum spread is reduced to $\Delta p / p = \num{1.4(2)e-4}$.
After the electron cooling is switched off with the electron cooler's solenoid remaining at its nominal field strength, \num{5e9} particles are stored in \gls{cosy} for use by the experiment.
The slip factor was measured to be $\eta = \num{0.533(1)}$.
The measurements presented in this section were carried out after the electron cooling but without prior orbit correction.



\subsection{Measurement stability}

The chromaticity measurement is tested with a positive symmetric frequency sweep of $\Delta f_\mathrm{rev}/f_\mathrm{rev} = \SI{0.3}{\permille}$ amplitude over \SI{2}{\second}.
The \gls{stft} is performed on segments of $N_\mathrm{seg}=2^{13}=\num{8192}$ consecutive position measurements.
\Cref{fig:chromaticity:jedi} shows the measured linear tune change.
The tune resonance peak in the spectrum is narrow due to the small momentum spread of the cooled beam.
A comparison to a measurement without cooling is given in \cref{app:cooling} where a much wider peak is observed.
Even without cooling, the slope of the linear fit can be determined with high precision and the measured chromaticities agree within their uncertainties.
\\


\begin{figure}[b!]
	\centering
	\includegraphics[scale=0.75]{data/chromaticity_jedi_1}
	\caption[Chromaticity measurement of a cooled \SI{970}{MeV/c} deuteron beam]{
		Tune as function of frequency change for a chromaticity measurement of a cooled \SI{970}{MeV/c} deuteron beam.
		The linear fit gives a frequency change of $\Delta f_\mathrm{rev}/f_\mathrm{rev} = \SI{0.29776(4)}{\permille}$, a chromaticity of $\xi_x=\num{-15.4(3)}$, $\xi_y=\num{6.5(3)}$ and a tune of $Q_x=\num{3.54856(14)}$, $Q_y=\num{3.59453(14)}$ at $\Delta f_\mathrm{rev}/f_\mathrm{rev}=0$.
	}
	% Timestamp: 2020-08-14 13:44:39.788920+00:00
	% Eta: 0.5330(10)
	% Frequency change: 223.472(16) Hz or 0.29776(4) ‰
	% X Tune: 3.55286(13) ... 3.54426(18) ; Change: -8.60(13) ‰ ; Mean: 3.54856(14)
	% Y Tune: 3.59271(13) ... 3.59635(18) ; Change: +3.64(13) ‰ ; Mean: 3.59453(14)
	% X Xi/eta: -28.9(5) --> X Xi: -15.4(3)
	% Y Xi/eta:  12.2(5) --> Y Xi:   6.5(3)
	\label{fig:chromaticity:jedi}
\end{figure}



Furthermore, the duration, amplitude and polarity of the frequency sweep is varied to examine the stability and systematic uncertainty of the chromaticity measurement.
\Cref{fig:chromaticity:stability} shows the results of a number of measurements in subsequent machine cycles during which the properties of the frequency sweep were varied.
The standard deviation of the measured values agrees with the uncertainty of a single chromaticity measurement (\cref{fig:chromaticity:jedi}), such that the reproducibility is confirmed.

\begin{figure}
	\centering
	\includegraphics[scale=0.75]{data/chromaticity_jedi_stability}
	\caption[Reproducibility of chromaticity and tune measurements]{
		Measured chromaticity (left) and tune (right) for frequency sweeps of different polarity, amplitude (\SIrange{0.3}{0.6}{\permille}) and duration (\SIrange{1}{2}{s}).
		The average value and standard deviation is indicated.
	}
	\label{fig:chromaticity:stability}
\end{figure}


By increasing the amplitude of the sweep by a factor of 2, the measurement uncertainty can be halved.
On the other hand, an increased sweep duration does not improve the accuracy of the measurement.




\subsection{Chromaticity compensation with sextupoles}
\label{sec:measure:chroma:compensation}

As described in \cref{sec:chromaticity_compensation}, sextupole magnets can be used to adjust and compensate the chromaticity.
This is demonstrated by comparing two measurements with and without compensation.

\Cref{fig:chromaticity:compensation:before} shows a chromaticity measurement with all sextupole magnets switched off.
The observed tune change is dominantly linear and the determined chromaticity is comparatively large with a value of $\xi_x=\num{-15.7(3)}$ and $\xi_y=\num{7.43(3)}$.

For the second measurement the sextupole magnets \enquote{MXG}, which are located in the apexes of the COSY ring, are energized with \SI{20}{\percent} of the maximum current. % apexes/vertices/zeniths
It is known from operational experience that this setting is suited to largely compensate the chromaticity.
As can be seen in \cref{fig:chromaticity:compensation:after}, the tune change is indeed smaller and the chromaticity is reduced to $\xi_x=\num{-1.4(3)}$ and $\xi_y=\num{-1.13(9)}$.
Due to the small linear component, non-linear tune changes become apparent.
These higher order terms are refereed to as non-linear chromaticity~\cite[517]{Wiedemann.2015}.
\\

To quantify the non-linearity of the measurement, a polynomial of cubic degree is fitted to the data:
\begin{equation}
\label{eq:chromaticity:nonlinear}
	Q %\left(\frac{\Delta f_\mathrm{rev}}{f_\mathrm{rev}}\right) 
	= Q_0
	  + \xi\cdot\frac{\Delta f_\mathrm{rev}}{\eta f_\mathrm{rev}}
	  + \xi^{(2)}\cdot\left(\frac{\Delta f_\mathrm{rev}}{\eta f_\mathrm{rev}}\right)^2
	  + \xi^{(3)}\cdot\left(\frac{\Delta f_\mathrm{rev}}{\eta f_\mathrm{rev}}\right)^3
\end{equation}
with the tune $Q_0$ for zero momentum deviation, the chromaticity $\xi$, and the second and third order non-linear components $\xi^{(2)}$ and $\xi^{(3)}$ as fit parameters.

The optimized values of the non-linear fit parameters significantly depend on the degree of the polynomial.
To quantify this effect, the degree was varied from \numrange{3}{6} and the standard deviation of multiple fit results is used as systematic uncertainty.
The resulting parameters and their total uncertainties for both measurements are given in \cref{tab:chromaticity:fitresults}.
\\


By comparing the linear fit component~$\xi$ of both measurements, it is apparent that the sextupole magnets indeed lead to a reduction of the chromaticity in both planes.
It can also be seen, that the measured tunes were affected by the sextupoles.
This is because the orbit was not corrected at the time of the measurement, such that the beam passed the sextupole fields off-centre.
Therefore, the sextupoles have a non-zero focusing strength even for particles with vanishing momentum deviation, which leads to a shift in the tune.

The quadratic contribution to the chromaticity $\xi^{(2)}$ are only slightly affected by the sextupole fields.
Only in the horizontal plane a difference larger than the uncertainty was measured, which might be explained by higher order components of the magnetic field.
The cubic coefficients $\xi^{(3)}$ are all zero within the measurement uncertainty and therefore negligible.
\\

In summary, the chromaticity measurement and the concept of chromaticity compensation with sextupole magnets were successfully demonstrated.










\begin{figure}[p]
	\centering
	\includegraphics[scale=0.75]{data/chromaticity_compensation_1}
	\caption[Chromaticity measurement with sextupole magnets off]{
		Tune as function of frequency change for a chromaticity measurement with sextupole magnets switched off (uncompensated).
		The polynomial fit (dashed line) is dominated by the linear component.
	}
	\label{fig:chromaticity:compensation:before}
\end{figure}

\begin{figure}[p]
	\centering
	\includegraphics[scale=0.75]{data/chromaticity_compensation_2}
	\caption[Chromaticity measurement with sextupole magnets on]{
		Tune as function of frequency change for a chromaticity measurement with sextupole magnets switched on.
		The chromaticity is mostly compensated causing the linear component in the polynomial fit (dashed line) to vanish while higher order correlations become apparent.
	}
	\label{fig:chromaticity:compensation:after}
\end{figure}

\begin{table}[p]
	\centering
	\def\arraystretch{1.1}
	\begin{tabular}{lc S[table-format=1.5(1)]S[table-format=3.2(1)]S[table-format=2.1(2)E1]S[table-format=2.1(1)E1]}
		\multicolumn{2}{l}{} & {$Q_0$}  & {$\xi$}       & {$\xi^{(2)}$} & {$\xi^{(3)}$} \\ \hline
		
		\multirow{2}{*}{\shortstack[l]{uncompensated}}
		
		%		    X w/o: 	Q0 = 3.549099 ± 0.000008 ± 0.000023 (sys)
		%					xi = -15.71 ± 0.05 ± 0.25 (sys)
		%					xi²= 2050 ± 60 ± 920 (sys)
		%					xi³= 1680000 ± 180000 ± 3800000 (sys)
		& Hor. & 3.54910(3)             & -15.7(3)           & 2.1(10)E3         &  2(4)E6      \\ %\cline{2-6}
		
		%		    Y w/o:  Q0 = 3.595560 ± 0.000004 ± 0.000022 (sys)
		%					xi = 7.43 ± 0.03 ± 0.02 (sys)
		%					xi²= -1130 ± 30 ± 670 (sys)
		%					xi³= -1560000 ± 100000 ± 160000 (sys)
		& Ver. & 3.59556(3)             &   7.43(3)          &-1.1(7)E3          & -1.6(2)E6    \\ \hline
		
		\multirow{2}{*}{\shortstack[l]{compensated}}
		
		%		    X with: Q0 = 3.561438 ± 0.000003 ± 0.000022 (sys)
		%					xi = -1.35 ± 0.11 ± 0.25 (sys)
		%					xi²= 5600 ± 900 ± 1000 (sys)
		%					xi³= -2000000 ± 500000 ± 4000000 (sys)
		& Hor. & 3.56144(3)             &  -1.4(3)           & 5.6(10)E3         & -2(4)E6      \\ %\cline{2-6}
		
		%		    Y with: Q0 = 3.5873356 ± 0.0000013 ± 0.0000246 (sys)
		%					xi = -1.13 ± 0.09 ± 0.02 (sys)
		%					xi²= -1800 ± 300 ± 1300 (sys)
		%					xi³= -1000000 ± 300000 ± 200000 (sys)
		& Ver. & 3.58734(3)             &  -1.13(9)          &-1.8(13)E3	      & -1.0(3)E6  \\
		
	\end{tabular}
	\caption[Fit results for the chromaticity measurements]{
		Results for the cubic polynomial fits of the chromaticity measurements with and without compensation by sextupole magnets.
		Besides tune~$Q_0$ and chromaticity~$\xi$ also the quadratic~$\xi^{(2)}$ and cubic~$\xi^{(3)}$ coefficients are given according to \cref{eq:chromaticity:nonlinear}.
		The uncertainties in parentheses are the combined statistical and systematic uncertainties.
	}\label{tab:chromaticity:fitresults}
\end{table}


