% !TeX spellcheck = en_GB


\section{RF frequency sweep}
\label{sec:rf_sweep}

In general, the frequency change --- and therefore the momentum change --- should be as large as possible to increase the sensitivity to small values of the chromaticity.
However, the amplitude of the change is limited by several aspects:
\begin{compactlist}
	\item Since the linear chromaticity is to be measured, the change must be small enough such that the beam dynamics stay in the linear regime\footnote{
		In extension to the linear regime, a non-linear chromaticity can be defined, taking higher order tune changes into account~\cite[517]{Wiedemann.2015}.
		See also \cref{sec:measure:chroma:compensation}.
	}
	\item A momentum change at fixed bending fields results in a dispersive orbit change, which has to be kept small in order to stay within the accelerator's acceptance
	\item If the change in tune is too large, the beam can become unstable as betatron resonances might be hit
\end{compactlist}
%
According to \cite[348]{Steinhagen.2009}, a relative momentum change $\Delta p/p$ in the range of \numrange{e-4}{e-3} is to be favoured.
The corresponding frequency change for a typical slip factor of $\eta\approx0.6$ for \gls{cosy} is in the sub-permille range. % (a few \SI{100}{Hz} for a typical revolution frequency of $f_\mathrm{rev}\approx\SI{1}{MHz}$).
While preliminary tests showed non-linearities for larger frequency changes in the order of $\Delta f/f\approx\pm\SI{1}{\permille}$, changes of about \SI{\pm.3}{\permille} proved adequate.


The duration of the frequency sweep is chosen as a trade-off between excitation induced beam losses and a sufficient resolution in time for the linear regression.
A sweep duration of about \SIrange{0.5}{1}{s} was found to be adequate, longer sweep durations do not improve the measurement results but lead to an increased beam loss.
% On the other hand, for sweeps as fast as \SI{500}{Hz} in \SI{.04}{s} an insufficient bunching and vanishing tune signal was observed.
Therefore, a symmetric frequency sweep of amplitude \SI{.6}{\permille} (\SI{\pm.3}{\permille} with respect to the nominal frequency) and \SI{1}{s} duration is suggested and used as default for the chromaticity measurement.
In contrast to a unipolar asymmetric sweep, a bipolar symmetric sweep allows for a twice as large amplitude and is to be preferred.
\\



At \gls{cosy}, all synchronously ramped components are described as a function of time by piecewise polynomials.
These are calculated prior to operation and stored in so called \emph{fgen} files (see \cref{app:fgen}).
%
The fgen files for the \gls{rf} are loaded to a frequency generator, which outputs a variable sinusoidal signal according to the predefined frequency, phase and amplitude. % when being triggered.
After amplification, this signal is wave-guided to the \gls{rf} cavity where the beam is bunched and accelerated.

For a chromaticity measurement, the bunched beam is temporarily accelerated by a precise linear frequency sweep.
This is realized by adding corresponding linear segments to the fgen file.
%
A dedicated script was developed to simplify the setup of the required frequency sweeps.
This script reads the frequency fgen file and parses its contents.
The polynomial at the specified cycle time is cut such that the sweep can be inserted.
Given the sweep duration and its relative amplitude, three linear polynomials are added, creating a symmetric sweep as depicted in \cref{fig:sweep}.
The actual measurement takes place only during the rising flank.
This process is repeated for each specified cycle time, allowing for multiple chromaticity measurements in a single machine cycle.
Afterwards, the modified fgen file is saved and sent to the frequency generator as usual.



\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=0.8]
		\def\f{1.6} % reference frequency
		\def\t{4} % time
		\def\d{3} % duration
		\def\a{2} % amplitude
		\def\W{11.5} % width of plot
		\def\H{3} % height of plot
		
		\draw[thin, black,-latex] (0,0) -- (\W,0);
		\node[text=black,anchor=north east] at (\W,0) {$t$};
		\draw[thin, black,-latex] (0,0) -- (0,\H);
		\node[text=black,anchor=north east] at (0,\H) {$f$};
		
		\draw[dashed, gray] (\t+0.2,\f-\a/2) -- (\t-0.5,\f-\a/2);
		\draw[dashed, gray] (\t+\d+0.2,\f+\a/2) -- (\t-0.5,\f+\a/2);
		\draw[dashed, gray] (\t+\d,\f+\a/2+0.2) -- (\t+\d,\f-\a/2-0.5);
		\draw[dashed, gray] (\t,\f-\a/2+0.2) -- (\t,-0.2);
		
		\draw[ultra thick,Green] (0,\f) -- (\t-\d/2,\f) -- (\t,\f-\a/2) -- (\t+\d,\f+\a/2) -- (\t+\d*3/2,\f) -- (\W-0.5,\f);	
		
		\draw[>=latex, <->, thick] (\t-0.3,\f-\a/2) -- (\t-0.3,\f+\a/2);
		\node[text=black,anchor=south east] at (\t-0.3,\f) {amplitude};
		\draw[>=latex, <->, thick] (\t,\f-\a/2-0.3) -- (\t+\d,\f-\a/2-0.3);
		\node[text=black,anchor=south] at (\t+\d/2,\f-\a/2-0.3) {duration};		
		\node[text=black,anchor=north] at (\t,-0.1) {time};
	\end{tikzpicture}
	\caption{
		Symmetric frequency sweep
	}
	\label{fig:sweep}
\end{figure}
