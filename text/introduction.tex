% !TeX spellcheck = en_GB



\section{The Cooler Synchrotron COSY}
\label{sec:introduce_cosy}


\begin{figure}[b!]
	\centering
	\includegraphics[width=0.88\textwidth]{graphics/cosy}
	\caption[Accelerator complex at the \glsentryfull{ikp}]{
		Accelerator complex at the \glsentryfull{ikp}%, Forschungszentrum Jülich
	}\label{fig:cosy}
\end{figure}

The \gls{ikp} at Forschungszentrum Jülich~\cite{fzj.de/ikp} operates the accelerator complex depicted in \cref{fig:cosy}.
Polarized or unpolarized particle beams are produced in an ion source and accelerated to an energy of \SI{45}{MeV} in the \gls{julic}.
Besides being used for isotope production and studies at the low energy irradiation place, the ion beam can be injected into the \gls{cosy}.
\Gls{cosy} is a storage ring with a circumference of \SI{183.47}{m}, capable of accelerating proton and deuteron beams in the momentum range from \SI{300}{MeV/c} up to \SI{3.7}{GeV/c} \cites{Dietrich.2004}{fzj.de/cosy}.
Two electron cooling devices for the low and high energy range as well as a stochastic cooling system ensure excellent beam quality and long beam lifetimes.
The beam can be stored and used for internal experiments, or it can be extracted to three external experimental areas.
%\\

The research and development at \gls{ikp} focuses on three main activities~\cite[17]{IKP.2019}:
\begin{compactlist}
	\item Searches for an \gls{edm} \cite{EDM.2019}.
	\item Construction of the \gls{hesr} \cite{Lehrach.2005}, a proton and heavy ion accelerator being build for the future \gls{fair} at the \gls{gsi}.
	%This includes component and detector test carried out at \gls{cosy}.
	\item Activities towards the \gls{hbs}~\cite{HBS.2020}, a neutron research centre planned to provide neutron beams from a proton accelerator.
\end{compactlist}



\section{Searches for an electric dipole moment}
\label{sec:edm_search}
\glsreset{edm}\glsreset{jedi}

% what is an EDM
The \gls{edm} quantifies the separation of a particles charge along its principal axis.
A subatomic particle with a \emph{non-zero} permanent \gls{edm} would violate the time reversal symmetry ($T$).
Violation of $T$ is expected from the known violation of $CP$ (charge and parity symmetry) in weak interactions according to the $CPT$-Theorem.
However, a $T$-violating \gls{edm} has not been observed so far and only upper limits for its existence have been measured.
\cite[134\psq]{Griffiths.1987}

% why important to measure
The \gls{sm} predicts \glspl{edm} smaller than \SI{e-31}{\elementarycharge\centi\meter}, which is orders of magnitude below the measured limits.
Several theories have been proposed that predict higher values for the \gls{edm} near the current limits.
A successful \gls{edm} measurement would therefore provide a test for physics beyond the \gls{sm}.
This would shine a light on challenging questions in modern physics:
The matter--antimatter asymmetry of the universe can not be explained with the currently known $CP$ violating processes.
Furthermore, the observation of an oscillating \gls{edm} would support existing theoretical models for dark-matter candidates.
\cite[1\psqq]{EDM.2019}
\\

% jedi
The \gls{jedi} collaboration aims to extend the existing limit on the \gls{edm} for protons and accomplish the first measurement for deuterons.
Therefore, precursor experiments and \gls{edm} measurements on deuterons are carried out at \gls{cosy}.
These pave the way for a precision measurement of the proton \gls{edm} with a dedicated future storage ring.
\cites{EDM.2019}{Andres.2020}{jedi.de}

% measurement principle
The storage ring is thereby used as a particle trap.
Its magnetic and electric\footnotemark{} fields act on the spin of the trapped particles according to the Thomas-BMT-Equation, causing the spin axis to precess.
% While the precession due to the magnetic fields is in the horizontal plane (around a vertical precission axis), an \gls{edm} will couple to the motional electric field and cause a precession in the vertical plane (around a horizontal precission axis), thus tilting the precession axis.
A non-vanishing \gls{edm} will cause the precession axis to tilt, producing a vertically oscillating spin component~\cite{Farley.2004}. %  proportional to the \gls{edm}
The \gls{edm} is then determined by measuring the vertical polarization built-up of a spin polarized beam. % from the horizontal asymmetry of a scattering process
Since the oscillation is proportional to the very small \gls{edm}, a measurement time in the order of \SI{15}{\minute} is required, during which the spin must stay coherent~\cites[40\psqq]{EDM.2019}.
% The precession leads to a smear out of the spins around the precession axis (spin incoherence).
Therefore, one of the key requirements for such a measurement is to achieve a sufficiently long spin coherence time (polarization lifetime).

\footnotetext{In the particle rest frame the magnetic guiding fields produce motional electric fields $\vec{E}=\gamma \vec{v}\cross\vec{B}$}





\section{Motivation and objective}


% Motivation


% general need for a tune and chromaticity measurement
Beam diagnostic is crucial for operation of an accelerator.
The betatron tune (working point) and its dependence on the momentum (chromaticity) are two central quantities of interest -- not only for the operating crew but also for experiments.
They must be monitored to avoid resonances which cause beam instabilities and limit the lifetime of the stored particle beam.

For polarized beams additional intrinsic spin tune resonances also limit the polarization lifetime.
To achieve sufficiently long spin coherence times, which are required for an \gls{edm} measurement as described in \cref{sec:edm_search}, control of the tune and chromaticity is essential~\cite{Guidoboni.2018}.
This requires a precise measurement of these quantities multiple times per machine cycle.
\\

% existing system and need or a new development
The existing tune measurement system at \gls{cosy} (tune sweep system) uses a \glsentrylong{btf} measurement~\cite{IKP.2016.ShortReports.TuneMeter}.
With a measurement time of a few seconds, such a method is comparably slow by design and not capable of continuous tune tracking e.g. during the acceleration.
A formerly used dynamic measurement system~\cite{Dietrich.1998} has already reached the end of its life and can no longer be utilized.

The standard procedure for chromaticity measurements at \gls{cosy} involves manual adjustment of the momentum and subsequent tune measurements over several machine cycles.
This makes it a complex and time-consuming task not suited for routine monitoring and especially not for the systematic chromaticity scans envisaged by the \gls{jedi} collaboration.

Therefore, the need for a fast and automated tune and chromaticity measurement system arose, that is capable to fulfil the requirements by the operating crew and the \gls{jedi} collaboration.
\\



% Objective


% expoit existing capabilities
In 2017 the \gls{bpm} system of \gls{cosy} was upgraded and new readout electronics were installed.
These can measure the beam position of every particle bunch passing the \gls{bpm} with a precision of \SI{100}{\micro\meter}~\cites{Boehme.2017}{Kamerdzhiev.2018}.
Already a few milliseconds of the bunch-by-bunch position data is in principle sufficient to determine the betatron tune.

Exploiting these capabilities, a fast and robust tune measurement system will be developed.
The key requirements on the new system are:
\begin{compactlist}
	\item Measurement of the betatron tune for bunched beams
	\item Full beam energy range (\SI{45}{MeV} to \SI{2.7}{GeV})
	\item Measurement time below \SI{100}{\milli\second}
	\item Accuracy of \num{e-3} (or better)
	\item Multiple measurements per machine cycle
\end{compactlist}
In addition, continuous measurements during the acceleration ramp are desired.
\\

Based on the tune measurement an automated chromaticity measurement system will be developed.
The key requirements are:
\begin{compactlist}
	\item Measurement of the \emph{total} chromaticity taking the effect of sextupole magnets into account
	\item At least one, preferably three measurements per machine cycle
	\item Sensitive to values close to zero chromaticity
\end{compactlist}


% envisaged usage of the measurement systems
The tune and chromaticity measurement systems will be used by the \gls{jedi} collaboration in scope of precursor experiments carried out at \gls{cosy}.
Systematic measurements will be performed to study the dependence of the spin coherence time on the horizontal and vertical chromaticity and tune.
Thereby the working point providing the longest possible spin coherence time can be found.




