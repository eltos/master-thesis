% !TeX spellcheck = en_GB

\documentclass[titlepage,a4paper,twoside,12pt,bibliography=totoc,BCOR=6.5mm,english,final]{scrreprt}
\pdfcatalog{/PageLayout /TwoPageRight}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[usenames, dvipsnames, svgnames, table]{xcolor}
\usepackage{amsfonts}
\usepackage[intlimits]{amsmath}
\usepackage{mathtools}
\usepackage{cancel}
\usepackage{physics}
\usepackage{bm}
\usepackage[main=english, ngerman]{babel}
\usepackage[backend=biber, style=alphabetic]{biblatex}
\usepackage{expdlist}
\usepackage{enumitem}
\usepackage[pdftex]{graphicx}
\usepackage{rotating}
\usepackage{subcaption}
\usepackage{floatrow} \floatsetup[table]{capposition=top}  % caption above table
\usepackage{longtable}
\usepackage{minted}
\usepackage[skins,minted,breakable]{tcolorbox}
\usepackage{MnSymbol}
\usepackage[acronym,toc,nomain,nonumberlist,nopostdot]{glossaries}
\usepackage[section]{placeins}
\usepackage[nottoc]{tocbibind}
\usepackage[binary-units]{siunitx}
\usepackage[automark, headsepline]{scrlayer-scrpage}
\usepackage{tabu}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{titling}
\usepackage{tikz}
\usepackage{xspace}
\usetikzlibrary{calc}
\usetikzlibrary{positioning}
\usepackage[english]{varioref}
%\usepackage[pdftex]{hyperref} % ocgcolorlinks --> don't print colored links --> but then url line-breaks do not work anymore :( --> use ocgx2 (see below)
\usepackage{bookmark} % this also loads hyperref
\usepackage{appendix}
\usepackage{import}
\usepackage[babel]{csquotes}
\usepackage{etoolbox}
\usepackage[scaled=.90]{helvet}
\usepackage[noabbrev]{cleveref}
\usepackage[ocgcolorlinks]{ocgx2}
\usepackage{nextpage}
\usepackage{accsupp}
\usepackage{draftwatermark}\SetWatermarkLightness{0.98}\SetWatermarkText{\BeginAccSupp{method=escape,ActualText={}}DRAFT\EndAccSupp{}}  % non-selectable watermark


% customisation
\include{preface/header}

% package setup
\sisetup{locale=UK, per-mode=symbol}  % separate-uncertainty = true
\hypersetup{colorlinks=true, linkcolor=blue, citecolor=Green, urlcolor=Blue, pdfborder={0 0 0}, breaklinks=true} % bookmarksopen=true

% graphics
\graphicspath{{graphics/}}
\pdfpxdimen=0.0072in % 139dpi % textwidth ~ 820px

% options
\allowdisplaybreaks
\setlength\parindent{0pt}
\setacronymstyle{long-short}
\creflabelformat{equation}{#2#1#3}
% controls the level of granularity of the table of contents
\setcounter{tocdepth}{4}
\setcounter{secnumdepth}{4}
\hypersetup{bookmarksdepth=4}


% custom commands: shortcuts for common words
\newcommand{\betafun}{betatron function}
\newcommand{\pickup}{pick-up} \newcommand{\Pickup}{Pick-up}
\newcommand{\st}{\textsuperscript{st}}
\newcommand{\nd}{\textsuperscript{nd}}
\newcommand{\rd}{\textsuperscript{rd}}
\renewcommand{\th}{\textsuperscript{th}}
\newcommand{\bbb}{bunch-by-bunch}
\newcommand{\tune}[1]{\num[separate-uncertainty=true]{#1}}



% Hyphenations
\hyphenation{Syn-chro-tron-strah-lungs-quel-len}



% Glossary
\include{preface/glossary}

% Bibliography
\addbibresource{bibliography/literatur.bib}%





% title
\author{Philipp Niedermayer}
\title{% max 3 lines
	Development of a Fast Betatron Tune \\
	and Chromaticity Measurement System \\
	Using Bunch-by-Bunch Position Monitoring
}
\date{Aachen, May 2021}
\courseofstudies{Master Thesis \\Physics}
\university{\uppercase{RWTH Aachen}}  % Rheinisch-Westfälische Technische Hochschule Aachen
\department{Submitted to the III. Institute of Physics B at the\\
	Faculty for Mathematics, Computer Science and Natural Science}
\logo{graphics/logo_rwth_physik3b}


\hypersetup{pdftitle={\thetitle},pdfauthor={\theauthor},pdfsubject={\thecourseofstudies},
	pdfkeywords={}}




\begin{document}





% Preface
%%%%%%%%%%


\pagenumbering{alph}
\pagestyle{empty}

% Titlepage
\include{preface/titlepage}

% Delaration
\cleardoublepage
\include{preface/declaration}

% Abstract
\cleardoublepage
\pdfbookmark{Abstract}{Abstract}
\include{preface/abstract}



\cleardoublepage
\pagestyle{scrheadings}
\pagenumbering{roman}
\setcounter{page}{1}


% Table of contents
\pdfbookmark[-1]{\contentsname}{\contentsname}
\tableofcontents

% List of acronyms
\cleardoublepage
\printglossary[type=\acronymtype,title={Acronyms},style=acronyms]

% List of symbols
%\clearpage
\printglossary[type=symbolslist,style=symbunitlong,nogroupskip]

% List of figures and tables
\clearpage
{\setcounter{tocdepth}{1}  % restore customized TOC depth
\renewcommand{\addvspace}[1]{}  % remove spacing between chapters
\listoffigures
\let\clearpage\relax  % have both lists on the same page
\vspace{-1em}  % reduce spacing between LoT and LoF if on the same page
\listoftables
}




% Chapters
%%%%%%%%%%%


\cleardoublepage %\clearnewdoublepage
%\addtocontents{toc}{\protect\newpage} % adds a manual pagebreak to the TOC
\bookmarksetup{startatroot}
\pagenumbering{arabic}
\setcounter{page}{1}




\chapter{Introduction}
\label{introduction}
\input{text/introduction}


\cleardoublepage
\chapter{Theory of Accelerator Physics}
\label{theory}
\input{text/theory_beam_dynamic_transverse}
\clearpage
\input{text/theory_beam_dynamic_longitudinal}


\cleardoublepage
\chapter{Beam Diagnostics}
\label{diagnostics}
\input{text/theory_pick_up}
\input{text/theory_tune_measurement}
\input{text/theory_chromaticity_measurement}


\cleardoublepage
\chapter{Development of a Fast Tune Measurement System}
\label{dev_tune}
\input{text/development_tune_principle}
\input{text/development_tune_excitation}
\input{text/development_tune_data_analysis}
\input{text/development_tune_gui}


\cleardoublepage
\chapter{Development of a Chromaticity Measurement System}
\label{dev:chrom}
\input{text/development_chroma_principle}
\input{text/development_chroma_rf_sweep}
\input{text/development_chroma_data_analysis}
\input{text/development_chroma_gui}


\cleardoublepage
\chapter{Commissioning and Measurements at COSY}
\label{measurements}
\input{text/measurement_excitation_study}
\input{text/measurement_tune_control}
\input{text/measurement_tune_during_acceleration}
\input{text/measurement_chromaticity_and_compensation}


\cleardoublepage
\chapter{Conclusions}
\label{results}
\input{text/summary}
\input{text/outlook}


% Bibliography
\cleardoublepage
\sloppy
\printbibliography
\bigskip

The following publication related to this thesis is currently in preparation:
\begin{addmargin}[1.93cm]{0cm}
\medskip	P. Niedermayer et al.
			\enquote{Development of a Fast Betatron Tune and Chromaticity Measurement System for COSY}.
			Submitted to the \emph{12th International Particle Accelerator Conference (IPAC'21)}.

\end{addmargin}




%\cleardoublepage
%\chapter*{Acknowledgement}
\vfill
\section*{Acknowledgement}
\thispagestyle{empty}
\pagestyle{empty} % plain or empty
%\addcontentsline{toc}{chapter}{Acknowledgement}
\input{preface/acknowledgement}



% Postface
%%%%%%%%%%%


% Appendix
\setupappendix[notocsubsections]
\begin{appendices}

	\cleardoublepage\setcounter{page}{1}
	\chapter{Formula Derivations}
	\input{appendix/derivation_fft}
	\input{appendix/derivation_bjump}

	\cleardoublepage\setcounter{page}{1}
	\chapter{Implementation Details}
	\import{appendix}{fgen}
	\input{appendix/gui_screenshots}

	\cleardoublepage\setcounter{page}{1}
	\chapter{Measurements}
	\input{appendix/excitation_study}
	\input{appendix/tune_mqu_study}
	\input{appendix/chromaticity_study}
	\input{appendix/sweep_excitation}

	\clearpage

\end{appendices}



% Back page
\cleartoevenpage[\pagestyle{empty}]
\pagestyle{empty}
\null


\end{document}


