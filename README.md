# Development of a fast betatron tune and chromaticity measurement system using bunch-by-bunch position monitoring
**Philipp Niedermayer**  
Master thesis in physics at the RWTH Aachen University


[Download thesis](/../-/jobs/artifacts/master/raw/thesis.pdf?job=pdf) as PDF file

[Download colloquium slides](/../-/raw/master/Colloquium.pdf?inline=false) as PDF file
